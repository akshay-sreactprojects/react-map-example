# Map Example in React

This mini project demonstrates skills in:

- React.js
- Google Maps API
- Gitlab CI
- managing sensitive credentials

In order to start this project you could either click [here](https://akshay-sreactprojects.gitlab.io/react-map-example/), or you could follow the following steps:

<ol> 
    <li> Clone this project.
    <li> Add your Google api key in the *.env* file.
    <li> use *npm install* to install dependencies
    <li> use *npm start* to run the project in development.
</ol>
