const ViewTypes = {
  GOOGLE_MAP: "GOOGLE_MAP",
  OPEN_STREET_MAP: "OPEN_STREET_MAP",
  THEMES: "THEMES",
};

export default ViewTypes;
