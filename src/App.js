import React, { Component } from "react";
import GMap from "./Components/GoogleMap/Map.view";
import OSMap from "./Components/OpenStreetMap/Map.view";
import PersistentDrawerLeft from "./Components/AppBar/PersistentDrawerLeft.view";
import { createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";

import ViewTypes from "./Utils/Misc";

require("dotenv").config();

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#0288d1",
      main: "#0277bd",
      dark: "#01579b",
      contrastText: "#fff",
    },
    secondary: {
      light: "#ff7961",
      main: "#f44336",
      dark: "#ba000d",
      contrastText: "#000",
    },
  },
});
/**
 * The App class component
 * @component
 */
export class App extends Component {
  /**
   * Cunstructor for the app component.
   * @constructor
   * @param {JSON} props The properties recieved by this component.
   */
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: ViewTypes.GOOGLE_MAP,
    };
    this.setSelectedItem = this.setSelectedItem.bind(this);
    this.getSelectedItem = this.getSelectedItem.bind(this);
  }

  /**
   * Function to set the option selected
   * @param {string} option Selected option
   */
  setSelectedItem(option) {
    console.log("setSelectedItem:", option);
    this.setState({
      selectedItem: option,
    });
  }

  /**
   * Function to get the option currently selected
   * @return {string} Currently selected option
   */
  getSelectedItem() {
    return this.state.selectedItem;
  }

  /**
   * The render method as required in a class component
   * @return {JSX} the app component
   */
  render() {
    return (
      <div>
        <ThemeProvider theme={responsiveFontSizes(theme)}>
          <PersistentDrawerLeft
            getSelectedItem={this.getSelectedItem}
            setSelectedItem={this.setSelectedItem}
          />
          {this.getSelectedItem() === ViewTypes.GOOGLE_MAP && <GMap />}
          {this.getSelectedItem() === ViewTypes.OPEN_STREET_MAP && <OSMap />}
        </ThemeProvider>
      </div>
    );
  }
}

export default App;
