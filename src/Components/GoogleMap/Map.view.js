import React, { useState, useEffect } from "react";
import { Map, GoogleApiWrapper, Marker } from "google-maps-react";
import PropTypes from "prop-types";

require("dotenv").config();

const mapStyles = {
  width: "100%",
  height: "100%",
};

/**
 * The google map component
 * @component
 * @param {JSON} props The properties recieved by this component
 * @return {JSX} Google map with a marker on it
 */
function GMap(props) {
  const [lat, setLat] = useState(0);
  const [lng, setLng] = useState(0);
  const [pos, setPos] = useState(new window.google.maps.LatLng(0, 0));
  let mapRef;

  const requestLocation = () => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(function (position) {
        console.log("Latitude is :", position.coords.latitude);
        console.log("Longitude is :", position.coords.longitude);
        setLat(position.coords.latitude);
        setLng(position.coords.longitude);
        setPos(position);
        if (mapRef !== null) {
          mapRef.Marker.setPos(position);
        }
      });
      console.log("Available");
    } else {
      console.log("Not Available");
    }
  };

  useEffect(() => {
    requestLocation();
  });

  return (
    <div>
      <Map
        google={props.google}
        zoom={14}
        style={mapStyles}
        center={{
          lat: lat,
          lng: lng,
        }}
        ref={(ref) => {
          mapRef = ref;
        }}
      >
        <Marker
          // onClick={onMarkerClick}
          name={"This is test name"}
          position={{ lat: pos.lat, lng: pos.lng }}
        />
      </Map>
    </div>
  );
}
GMap.propTypes = {
  google: PropTypes.string.isRequired,
};
/* eslint "new-cap": 0 */
export default GoogleApiWrapper({
  apiKey: process.env.REACT_APP_GOOGLE_KEY,
})(GMap);
