import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import React from "react";
import "leaflet/dist/leaflet.css";

/**
 * The Open Street Map Component
 * @param {JSON} props The properties required by the Open Street Map Component
 * @return {JSX} Renders the Open Street Map
 */
function OSMap(props) {
  return (
    <div>
      <MapContainer
        style={{ width: "100%", height: "100vh" }}
        center={[51.505, -0.09]}
        zoom={13}
        scrollWheelZoom={false}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={[51.505, -0.09]}>
          <Popup>
            A pretty CSS3 popup. <br /> Easily customizable.
          </Popup>
        </Marker>
      </MapContainer>
    </div>
  );
}
OSMap.propTypes = {};
export default OSMap;
